from django import forms
from .models import Status,Subscribe
from django.forms import ModelForm

class Status_Form(forms.ModelForm):
	status_attrs = {
		'id' : 'status',
		'type': 'text',
		'class': 'form-control',
		'placeholder':'Type your status here..'
	}
	status = forms.CharField(label='Status', required = True, widget=forms.TextInput(attrs=status_attrs))

	class Meta :
		model = Status
		fields = ('status',)
		
class Subscribe_Form(forms.ModelForm):
	nama_attrs = {
		'id' : 'id_nama',
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Name'
    }
	email_attrs = {
		'id' : 'id_email',
        'type': 'email',
        'class': 'form-control',
        'placeholder':'Email'
    }
	pass_attrs = {
		'id' : 'id_password',
        'type': 'password',
        'class': 'form-control',
        'placeholder':'Password',
		'required pattern' : '[a-zA-Z0-9]+',
		'title' : 'Harus Alpha Numeric'
    }
	email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
	nama = forms.CharField(widget=forms.TextInput(attrs=nama_attrs))
	password = forms.CharField(min_length = 8 ,widget=forms.TextInput(attrs=pass_attrs))
	class Meta :
		model = Subscribe
		fields = ('email','nama', 'password',)
