from django.test import TestCase, Client
from django.urls import resolve
from .views import index, add_status, index_2, data_json, index_json, index_subscribe
from .models import Status
from .forms import Status_Form, Subscribe_Form
from django.http import HttpRequest
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class Lab6UnitTest(TestCase):
	def test_lab_6_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)
	
	def test_lab_6_url_exist_2(self):
		response = Client().get('/myprofile/')
		self.assertEqual(response.status_code, 200)
		
	def test_book_url_exist_2(self):
		response = Client().get('/books/')
		self.assertEqual(response.status_code, 200)
		
	def test_lab_6_url_not_exist(self):
		response = Client().get('/')
		self.assertNotEqual(response.status_code,404)
		
	def test_lab6_using_index(self):
		found = resolve('/')
		self.assertEqual(found.func ,index)
		
	def test_lab6_using_index_2(self):
		found = resolve('/myprofile/')
		self.assertEqual(found.func ,index_2)
		
	def test_lab9_using_index_json(self):
		found = resolve('/books/')
		self.assertEqual(found.func ,index_json)
		
	def test_lab9_using_data_json(self):
		found = resolve('/data_json/')
		self.assertEqual(found.func ,data_json)
		
	def test_lab_6_using_lab_6_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'lab_6.html')
		
	def test_lab_6_using_lab_6_template_2(self):
		response = Client().get('/myprofile/')
		self.assertTemplateUsed(response, 'my_profile.html')
		
	def test_lab_9_using_html_books(self):
		response = Client().get('/books/')
		self.assertTemplateUsed(response, 'book.html')
		
	def test_model_can_create_new_status(self):
        #Creating a new Status
		new_status = Status.objects.create(date=timezone.now(),status='Belajar ppw yuhu')

        #Retrieving all available activity
		counting_all_status = Status.objects.all().count()
		self.assertEqual(counting_all_status,1)
		
	def test_form_status_input_has_placeholder_and_css_classes(self):
		form = Status_Form()
		self.assertIn('class="form-control', form.as_p())
		self.assertIn('id="status', form.as_p())
		
	def test_landing_page_is_completed(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn("Hello, Apa kabar?", html_response)
		
class Lab6FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		self.selenium.implicitly_wait(25) 
		super(Lab6FunctionalTest,self).setUp()
			
	def tearDown(self):
		self.selenium.quit()
		super(Lab6FunctionalTest, self).tearDown()
	
	def test_input_status(self):
		selenium = self.selenium
        # Opening the link we want to test
		selenium.get('http://localhost:8000/')
        # find the form element
		status = selenium.find_element_by_name('status')
		submit = selenium.find_element_by_id('submit')

        # Fill the form with data
		status.send_keys('Coba-coba')

        # submitting the form
		submit.send_keys(Keys.RETURN)
		text = selenium.page_source
		self.assertIn('Status', selenium.title)
		self.assertIn('Coba-coba', text)
	
	def test_using_css_on_form(self):
		selenium = self.selenium
		selenium.get('http://imadeadisuryan.herokuapp.com/')
		form = selenium.find_element_by_name('status')
		self.assertEqual("form-control",form.get_attribute("class"))
		
	def test_using_css_on_button(self):
		selenium = self.selenium
		selenium.get('http://imadeadisuryan.herokuapp.com/')
		button = selenium.find_element_by_id('submit')
		self.assertEqual("btn btn-lg btn-block btn-info",button.get_attribute("class"))
	
	def test_align_form(self):
		selenium = self.selenium
		selenium.get('http://imadeadisuryan.herokuapp.com/')
		form = selenium.find_element_by_name('status')
		self.assertEqual({'x' : 0, 'y' : 0}, form.location)
		
	def test_align_button(self):
		selenium = self.selenium
		selenium.get('http://imadeadisuryan.herokuapp.com/')
		button = selenium.find_element_by_id('submit')
		self.assertEqual({'x' : 0, 'y' : 0}, button.location)
		
		




