from django.conf.urls import url
from django.urls import re_path,include
from .views import index,add_status, index_2, index_json, data_json, index_subscribe, add_subscriber, validate_email, index_login, logout_view
from . import views

urlpatterns = [
    url(r'^$', index, name='index'),
    url('myprofile/', index_2, name='index_2'),
    url('add_status/', add_status, name = 'add_status'),
	url('data_json/', data_json, name = 'data_json'),
	url('books/', index_json, name = 'book'),
	url('validate_email/', validate_email, name = 'validate_email'),
	url('registration/', index_subscribe, name='index_subscribe'),
	url('add_subscriber/', add_subscriber, name = 'add_subscriber'),
	url('loginform/', index_login, name = 'loginform'),
	url('logout/', logout_view , name = 'logout'),

	
	
]