from django.db import models
from datetime import datetime, date
from django.core.validators import MinLengthValidator

# Create your models here.
class Status(models.Model):
	date = models.DateTimeField(default = datetime.now())
	status = models.TextField(max_length = 300)

class Subscribe(models.Model):
	email = models.EmailField(error_messages={'unique': 'An account with this email exist.'})
	nama = models.CharField(max_length = 50)
	password = models.CharField(validators=[MinLengthValidator(4)], max_length = 20)
