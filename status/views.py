from django.shortcuts import render, redirect
from .models import Status, Subscribe
from datetime import datetime
from .forms import Status_Form, Subscribe_Form
from . import forms
from django.http import HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.urls import reverse
import requests

response = {}
def index(request):
	response['form'] = forms.Status_Form
	response['status'] = Status.objects.all()
	html = 'lab_6.html'
	return render(request, html, response)

def index_2(request):
    return render(request, 'my_profile.html')

def add_status(request):
	form = forms.Status_Form(request.POST)
	if form.is_valid():
		response['status'] = request.POST['status']
		status = Status(status = response['status'])
		status.save()
		return HttpResponseRedirect('/')
	else:
		form = forms.Status_Form()

	return render(request, 'lab_6.html', response)
	
@csrf_exempt	
def add_subscriber(request):
	response = {}
	if (request.method == "POST" or None):
		response['email'] = request.POST['email']
		response['nama'] = request.POST['nama']
		response['password'] = request.POST['password']
		subscriber = Subscribe(email = response['email'], nama = response['nama'], password = response['password'])
		subscriber.save()
	return JsonResponse({})
	
def index_json(request):
	return render(request, 'book.html')
	
def data_json(request):
	search = "https://www.googleapis.com/books/v1/volumes?q=quilting" 
	my_list = requests.get(search)
	json = my_list.json()
	return JsonResponse(json)
	
def index_login(request):
	return render(request, 'login.html')
	
def index_subscribe(request):
	forms = Subscribe.objects.all()
	response = {'form' : Subscribe_Form, 'result': forms}
	return render(request, "subscribe.html", response)

@csrf_exempt	
def validate_email(request):
	email = request.POST.get('email', None)
	data = {
		'is_taken': Subscribe.objects.filter(email=email).exists()
	}
	return JsonResponse(data)

def my_view(request):
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(request, username = username, password = password)
	if user is not None:
		request.session['user_login'] = username
		res =  HttpResponseRedirect('status:book')
		res.set_cookie('test', 'berhasil')
		res.set_cookie('username', username)
	else:
		return HttpResponseRedirect('/loginform')
		
def logout_view(request):
	logout(request)
	return HttpResponseRedirect('/books')
	





