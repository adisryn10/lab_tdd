# Generated by Django 2.0.9 on 2018-11-20 17:02

import datetime
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('status', '0002_auto_20181030_1204'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subscribe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(error_messages={'unique': 'An account with this email exist.'}, max_length=254, unique=True)),
                ('nama', models.CharField(max_length=50)),
                ('password', models.CharField(max_length=20, validators=[django.core.validators.MinLengthValidator(4)])),
            ],
        ),
        migrations.AlterField(
            model_name='status',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 11, 21, 0, 2, 56, 245229)),
        ),
    ]
